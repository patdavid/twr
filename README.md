# TWR

Public repository for the U.S. Navy [Torpedo Weapons Retrieval (TWR)][TWRMIL] ship drawings and plans.

These drawings are Public Domain. 

[TWRMIL]: https://www.navsea.navy.mil/Home/Warfare-Centers/NUWC-Newport/What-We-Do/Detachments/Narragansett-Bay-Test-Facility/Range-Crafts/TWR-841/
